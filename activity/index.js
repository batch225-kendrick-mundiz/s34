const express = require("express");
const app = express();
const port = 3000;
app.use(express.json());
app.use(express.urlencoded({extended: true}));

let users = [
    {"username" : "Kendrick"},
    {"username" : "Claire"},
    {"username" : "Adrian"}
];
app.get("/home", (req, res) =>{
    res.send("Welcome to Home!");
});



app.get("/users", (req, res) =>{
    res.json(users);
});

app.delete("/delete-user", (req, res) => {
    for(let i = 0; i < users.length; i++){
        if(req.body.username === users[i].username){
            delete users[i];
            message = `User ${req.body.username} deleted`;
        }else{
            message = `User ${req.body.username} not found`;
        }
    }
    res.send(message);
});

app.listen(port, () => console.log('Server running at port ' + port))