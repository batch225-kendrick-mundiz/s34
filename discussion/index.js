const express = require("express");

const app = express();

const port = 3000;
//setup for allowing the server to handle data from request
// allows your app to read json data
// methods used from express js are middlewares
app.use(express.json());

//allows your app to read data from forms
// by the default information recieved from the url can only be recieved as string or an array
// by applying the option of extended true this allows us to recieve information in other data types such as an object which we will use throughout our application

app.use(express.urlencoded({extended: true}));

app.get("/hello", (req, res) =>{
    res.send("Hello World!");
});

app.post("/hello", (req, res) =>{
    res.send(`Hello There ${req.body.firstname} ${req.body.lastname}!`);
});

let users =[];
app.post("/signup", (req,res) =>{
    console.log(req.body);
    if(req.body.username.length !== '' && req.body.password !== ''){
        users.push(req.body);
        res.send(`User ${req.body.username} has been added`);
    }else{
        res.send(`Username or password is empty`);
    }
});

app.put("/update-password", (req,res) =>{
    let message;
    for(let i = 0; i < users.length; i++){
        if(req.body.username == users[i].username){
            users[i].password = req.body.password;
            message = `User ${req.body.username}'s password has been updated`;
        }else{
            message = `User ${req.body.username} not found`;
        }
    }
    console.log(req.body);
    res.send(message);
});

app.get("/view-users", (req, res) =>{
    res.json(users);
});

app.listen(port, () => console.log('Server running at port ' + port))